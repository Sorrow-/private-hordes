<!doctype html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>MAP VIEW</title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
<div id="map">
<?php

function addPlayer()
{
	return "<div class=\"player\" style=\"margin: ".rand(1,14)."px 0 0 ".rand(1,14)."px;\"></div>";
}

function addBulding()
{
	return "<div class=\"building\"></div>";
}

$i = 1;

$posY = -8;

for($x = 1; $x <= 12; $x++)
{
	$posX = -3;

	echo "<div class=\"ligne\">\n";

	for($y = 1; $y <= 12; $y++)
	{
		$in = "";
		$text1 = "Désert ";
		$text2 = "";

		switch ($i)
		{
			case 100:
				$class = "city";
				$text1 = "Nom de la ville ";
				break;

			case 76:
			case 88:
			case 87:
			case 89:
			case 98:
			case 99:
			case 101:
			case 102:
			case 111:
			case 112:
			case 113:
			case 124:
				$class = "safe";
				$in = addPlayer();
				break;

			case 103:
			case 114:
			case 115:
				$class = "zombies1";
				$in = addBulding().addPlayer().addPlayer();
				$text1 = "Maison d'un citoyen ";
				$text2 = "Zombies isolés";
				break;

			case 110:
			case 122:
				$class = "zombies2";
				$text2 = "Meute de zombies";
				break;

			case 77:
			case 90:
				$class = "zombies3";
				$in = addBulding();
				$text1 = "Route barrée ";
				$text2 = "Horde de zombies";
				break;
			
			default:
				$class = "unknown";
				$text1 = "";
				break;
		}

		$text1 .= "[ $posX / $posY ]";

		if($text2 != "")
			$text2 = "<br /><font color='#ffff00'>$text2</font>";

		if($x <= 2)
			$pos = "n";
		else
			$pos = "s";

		if($y <= 3)
			$pos .= "w";
		elseif($y > 9)
			$pos .= "e";

		echo "<div class=\"case $pos $class\" title=\"$text1$text2\">$in</div>";

		$i++;
		$posX++;
	}

	echo "</div>\n";

	$posY++;
}

?>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="js/jquery.tipsy.js"></script>
<script>
	$('.s').tipsy({gravity: 's', html: true});
	$('.sw').tipsy({gravity: 'sw', html: true});
	$('.se').tipsy({gravity: 'se', html: true});
	$('.n').tipsy({gravity: 'n', html: true});
	$('.ne').tipsy({gravity: 'ne', html: true});
	$('.nw').tipsy({gravity: 'nw', html: true});
</script>
</body>
</html>